export const state = () => ({
  isUserLoggedIn: false,
  user: null
})

export const mutations = {
  login(state, user) {
    state.isUserLoggedIn = true;
    state.user = user;
  },
  logOut(state) {
    state.isUserLoggedIn = false;
    state.user = null;
  }
}